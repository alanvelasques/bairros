<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'wp_bairros');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$kU$I3lHDIJ*QhP]bA;MSL eP_:]/F7wS$j*~{Z(b2Y,?Zc7EnyJnRNS(h]zkMKx');
define('SECURE_AUTH_KEY',  ')?2KHD9j@J[)27wc/>0|[oxq0H|Lqf6KTRQkcn.wQ-J}Ns~X9-_!+ZD/=VS(.sSz');
define('LOGGED_IN_KEY',    '5f8##>]k}r7O`[~//TqY$T;nW^wG2O Y&[%ZXME9$U:{-t~u&m[RzXCFNe8ED)(]');
define('NONCE_KEY',        'n.&,xFkQ((lXc1[7z8t]boEiZWSzzo>OX~#n8*pYn6LO6]AAwl~z~N2zsSn><cd8');
define('AUTH_SALT',        'y#e6|m{)Bh2Cm,E3oK*,^z%60t,jX[F#:_1*|va(q:dYh>*|9J6Or5eYViuHyr11');
define('SECURE_AUTH_SALT', '&{X!m*kvjnk]^^+|`f*twm<+kVAgh|29t`9?)&o3IJmTt`TgPq+e#b09lo9!+cyV');
define('LOGGED_IN_SALT',   'T@4]E?e&Nrq0vDqe>&M(C*iPE}*$C-xiyVu+l/1J8Dc)cq0KaQ}*TywJPMWk4eb`');
define('NONCE_SALT',       'u]iv6d,PN*ta-cJ^;9i-B)7E@S*5[njiuTT``&ckB7+,~mBFLg9ibwf7yc;Ec<+<');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
